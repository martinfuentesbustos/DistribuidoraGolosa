package ar.edu.ungs.distribuidora.beans;

public class Tupla{

    CentroDeDistribucion centro;
    Integer suma;

    public Tupla(CentroDeDistribucion centro, Integer suma) {
        super();
        this.centro = centro;
        this.suma = suma;
    }

    public CentroDeDistribucion getCentro() {
        return centro;
    }

    public Integer getSuma() {
        return suma;
    }

}