package ar.edu.ungs.distribuidora.beans;

import java.io.Serializable;

public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;

	private String nombre;
	
	private Double latitud;
	
	private Double longitud;

	public String getNombre() {
		return nombre;
	}

	public Double getLatitud() {
		return latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

}
