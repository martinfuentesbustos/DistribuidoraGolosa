package ar.edu.ungs.distribuidora.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import ar.edu.ungs.distribuidora.algoritmos.Instancia;
import ar.edu.ungs.distribuidora.algoritmos.Solucion;
import ar.edu.ungs.distribuidora.algoritmos.Solver;
import ar.edu.ungs.distribuidora.algoritmos.strategy.CalculadorPorMediana;
import ar.edu.ungs.distribuidora.algoritmos.strategy.CalculadorPorSuma;
import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;

public class SolverTest {

	private Instancia ret;
	
	@Test
	public void resolverPorSuma() {
		Solver solver = new Solver(ret, new CalculadorPorSuma());
		Solucion solucion = solver.resolver(2);

		assertEquals(4925, solucion.getCostoTotal(), -3);
		assertEquals(2, solucion.getCentrosElegidos().size(), -3);

	}

	@Test
	public void resolverPorMediana() {
		Solver solver = new Solver(ret, new CalculadorPorMediana());
		Solucion solucion = solver.resolver(2);

		assertEquals(5106, solucion.getCostoTotal(), -3);
		assertEquals(2, solucion.getCentrosElegidos().size(), -3);

	}

	@Test
	public void centroMasCercanoTest() {
		Solver solver = new Solver(ret, new CalculadorPorSuma());
		Solucion solucion = solver.resolver(2);
		
		List<CentroDeDistribucion> listaEsperada = new ArrayList<CentroDeDistribucion>();
		CentroDeDistribucion c1 = new CentroDeDistribucion();
		c1.setNombre("Centro Antiguo Martin.com");
		c1.setLatitud(-34.54025641038163);
		c1.setLongitud(-58.71298910563181);
		
		CentroDeDistribucion c2 = new CentroDeDistribucion();
		c2.setNombre("Centro Anses");
		c2.setLatitud(-34.539815708025294);
		c2.setLongitud(-58.717537272274406);
		
		listaEsperada.add(c1);
		listaEsperada.add(c2);
		
		List<CentroDeDistribucion> listaActual = solver.centroMasCercano(solucion.getCentrosMasCercanos());
		
		assertTrue(listaActual.containsAll(listaEsperada));
	}
	
	@SuppressWarnings("unused")
	@Test(expected = IllegalArgumentException.class)
	public void resolverPorSumaNull() {
		Solver solver = new Solver(ret, new CalculadorPorSuma());
		Solucion solucion = solver.resolver(0);
	}
	
	
	

	@Before
	public void setUp() {
		ret = new Instancia();
	}

}
