package ar.edu.ungs.distribuidora.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import ar.edu.ungs.distribuidora.utils.Semiverseno;

public class SemiversenoTest {

	double lat1;
	double lon1;
	double lat2;
	double lon2;
	
	@Before
	public void setUp() throws Exception {
		lat1 = -34.54782823294682;
		lon1 = -58.71824853353916;
		lat2 = -34.542063;
		lon2 = -58.711986;
	}

	@Test
	public void testCalcularDistancia() {
		assertEquals(860, Semiverseno.calcularDistancia(lon1, lat1, lon2, lat2),-3);
	}
	
	@Test
	public void testCalcularDistanciaASiMismo() {
		assertEquals(0, Semiverseno.calcularDistancia(lon1, lat1, lon1, lat1),-3);
	}

}
