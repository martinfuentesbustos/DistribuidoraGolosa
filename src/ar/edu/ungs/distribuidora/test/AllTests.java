package ar.edu.ungs.distribuidora.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MedianaTest.class, SemiversenoTest.class, SolverTest.class })
public class AllTests {

}
