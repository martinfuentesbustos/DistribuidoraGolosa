package ar.edu.ungs.distribuidora.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import ar.edu.ungs.distribuidora.utils.Mediana;

public class MedianaTest {

	private ArrayList<Integer> distancias;
	
	@Before
	public void setUp() throws Exception {
		distancias = new ArrayList<>();
		distancias.add(860);
		distancias.add(654);
		distancias.add(1572);
		distancias.add(1192);
		distancias.add(1155);
		distancias.add(155);
		distancias.add(1063);
		distancias.add(1267);
	}

	@Test
	public void testCalcularMediana() {
		assertEquals(1109.0, Mediana.calcularMediana(distancias),-3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testCalcularMedianaNull() {
		assertEquals(1109.0, Mediana.calcularMediana(null),-3);
	}
	
	@Test
	public void testCalcularMedianaError() {
		assertFalse(Mediana.calcularMediana(distancias) == 121);
	}

}
