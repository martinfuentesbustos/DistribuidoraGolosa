package ar.edu.ungs.distribuidora.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ar.edu.ungs.distribuidora.algoritmos.Instancia;
import ar.edu.ungs.distribuidora.algoritmos.Solucion;
import ar.edu.ungs.distribuidora.algoritmos.Solver;
import ar.edu.ungs.distribuidora.algoritmos.strategy.Calculador;
import ar.edu.ungs.distribuidora.algoritmos.strategy.CalculadorPorMediana;
import ar.edu.ungs.distribuidora.algoritmos.strategy.CalculadorPorSuma;
import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Cliente;
import ar.edu.ungs.distribuidora.utils.Constantes;
import ar.edu.ungs.distribuidora.utils.ManejadorArchivos;
import ar.edu.ungs.distribuidora.utils.ManejadorMapa;
import ar.edu.ungs.distribuidora.utils.Utils;

public class DistribuidoraGolosaGUI extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private JPanel panelMapa;
	private Solver solver;
	private Instancia situacion;
	private Calculador calculador;
	private Solucion solucion;
	private JButton btnIniciar;
	private JLabel lblEstrategiaDeSolucion;
	private JComboBox<String> cboEstrategia;
	private JLabel lblCantidadDeCentros;
	private JSpinner spinnerCantidadCentrosDeseados;
	private JLabel label;
	private ArrayList<Coordinate> coordsClientes;
	private ArrayList<Coordinate> centroDecidido;
	
	private String[] menuCboEstrategia = {"Seleccionar...", "Por suma total", "Por c�lculo de mediana"};
	
	public DistribuidoraGolosaGUI() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(DistribuidoraGolosaGUI.class.getResource("/imagenes/icon_.png")));
		initialize();
	}

	private void initialize() {
		setTitle(Constantes.NOMBRE_APLICACION);
		this.setBounds(100, 100, 573, 541);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		lblEstrategiaDeSolucion = new JLabel("Estrategia de soluci�n");
		lblEstrategiaDeSolucion.setBounds(10, 11, 135, 17);
		
		cboEstrategia = new JComboBox<String>();
		cboEstrategia.setModel(new DefaultComboBoxModel<String>(menuCboEstrategia));
		cboEstrategia.setBounds(243, 9, 167, 20);

		btnIniciar = new JButton("Iniciar");
		btnIniciar.setEnabled(false);
		btnIniciar.setBounds(458, 11, 89, 49);

		lblCantidadDeCentros = new JLabel("Cantidad de centros que se desea abrir");
		lblCantidadDeCentros.setBounds(9, 41, 235, 19);

		spinnerCantidadCentrosDeseados = new JSpinner();
		spinnerCantidadCentrosDeseados.setBounds(243, 40, 55, 20);
		panelMapa = new JPanel();
		panelMapa.setVisible(false);
		panelMapa.setBorder(new LineBorder(new Color(0, 0, 0), 3));
		panelMapa.setBounds(new Rectangle(19, 71, 528, 384));
		getContentPane().add(panelMapa);
		panelMapa.setLayout(null);
		
		label = new JLabel("");
		label.setOpaque(true);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(19, 71, 528, 384);
		getContentPane().add(label);
		label.setIcon(new ImageIcon(DistribuidoraGolosaGUI.class.getResource("/imagenes/loading_1.gif")));
		getContentPane().add(lblEstrategiaDeSolucion);
		getContentPane().add(cboEstrategia);
		getContentPane().add(btnIniciar);
		getContentPane().add(lblCantidadDeCentros);
		getContentPane().add(spinnerCantidadCentrosDeseados);
		
		JLabel lblCosteDeLa = new JLabel("Coste de la soluci\u00F3n:");
		lblCosteDeLa.setVisible(false);
		lblCosteDeLa.setBounds(10, 466, 146, 26);
		getContentPane().add(lblCosteDeLa);
		
		JLabel lbl_coste = new JLabel("");
		lbl_coste.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbl_coste.setBounds(201, 466, 180, 26);
		getContentPane().add(lbl_coste);
		
		JButton btnGuardarEnArchivo = new JButton("Guardar en Archivo");
		btnGuardarEnArchivo.setEnabled(false);
		btnGuardarEnArchivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser fileChooser = new JFileChooser();
				int seleccion = fileChooser.showSaveDialog(getContentPane());
				
				if (seleccion == Constantes.GUARDAR_ARCHIVO)
				{
					List<CentroDeDistribucion> centros = solucion.getCentrosElegidos();
					Gson gson = new GsonBuilder().setPrettyPrinting().create();
					String json = gson.toJson(centros);
					ManejadorArchivos.guardar(fileChooser.getSelectedFile(), json);
				}
				
			}
		});
		btnGuardarEnArchivo.setBounds(374, 466, 173, 23);
		getContentPane().add(btnGuardarEnArchivo);

		cargarInstanciaDeDatos();
		configurarComponentes();
		inicializarJMap();
		
	

		
		//------------ LISTENERS --------------------------
		
		spinnerCantidadCentrosDeseados.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				habilitarBotonIniciar();
			}
		});
		
		cboEstrategia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				habilitarBotonIniciar();
			}
		});
		
		btnIniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				limpiarDatos();
				cargarInstanciaDeDatos();
				
				calculador = seleccionarEstrategia(cboEstrategia.getSelectedIndex());
				solver = new Solver(situacion, calculador);
				solucion = solver.resolver((int)spinnerCantidadCentrosDeseados.getValue());


				Cliente[] clientes = situacion.getClientes();
				CentroDeDistribucion[] centros = solucion.getCentrosElegidos().toArray(new CentroDeDistribucion[0]);
				List<MapMarker> marcadoresClientes = Utils.obtenerMarcadoresMapa(clientes, Constantes.ESTILO_CLIENTE);
				List<MapMarker> marcadoresCentros = Utils.obtenerMarcadoresMapa(centros,  Constantes.ESTILO_CENTRO);
				List<MapMarker> resultadoFinal = Stream.concat(marcadoresClientes.stream(), marcadoresCentros.stream()).collect(Collectors.toList());
				ManejadorMapa.getInstancia().crearConjuntoMarcadores(resultadoFinal);
				
				
				for(Cliente c : clientes) {
					Coordinate coordenada = new Coordinate(c.getLatitud(), c.getLongitud());
					coordsClientes.add(coordenada);
				}

				for(CentroDeDistribucion c : solucion.getCentrosMasCercanos()) 
				{
					Coordinate coordenada = new Coordinate(c.getLatitud(), c.getLongitud());
					 centroDecidido.add(coordenada);
				}
				
				ManejadorMapa.getInstancia().dibujarPoligono(coordsClientes, centroDecidido);

				lblCosteDeLa.setVisible(true);
				lbl_coste.setText(solucion.getCostoTotal().toString());
				btnGuardarEnArchivo.setEnabled(true);
			}
			
		});
		
	}


	//------------------------------------------------Metodos ----------------------------------------------
	
	private Calculador seleccionarEstrategia(int selectedIndex) {
		Calculador estrategia = null;
		
		if(selectedIndex == Constantes.OPCION_POR_SUMA)
			estrategia = new CalculadorPorSuma();
		if(selectedIndex == Constantes.OPCION_POR_MEDIANA)
			estrategia = new CalculadorPorMediana();
		return estrategia;
	}

	protected void limpiarDatos() {

		situacion = null;
		coordsClientes.clear();
		centroDecidido.clear();
		
		ManejadorMapa.getInstancia().removerMarcadores();
		ManejadorMapa.getInstancia().removerPoligonos();
	}

	private void habilitarBotonIniciar() {
		if(cboEstrategia.getSelectedIndex() != Constantes.OPCION_SELECCIONAR && (int)spinnerCantidadCentrosDeseados.getValue() > 0) {
			btnIniciar.setEnabled(true);
		}else
			btnIniciar.setEnabled(false);
	}

	private void configurarComponentes() {
		spinnerCantidadCentrosDeseados.setModel(new SpinnerNumberModel(0, 0, situacion.getCentros().length, 1));;
	}

	private void inicializarJMap() {
		ManejadorMapa.getInstancia().setearTamanio(Constantes.RECTANGULO_MAPA).visualizarControlesZoom(true)
		.establecerCentro(ManejadorMapa.CENTRO_MAPA, ManejadorMapa.DEFAULT_ZOOM_MAPA);		

		Cliente[] clientes = situacion.getClientes();
		CentroDeDistribucion[] centros = situacion.getCentros();
		
		List<MapMarker> marcadoresClientes = Utils.obtenerMarcadoresMapa(clientes, Constantes.ESTILO_CLIENTE);
		List<MapMarker> marcadoresCentros = Utils.obtenerMarcadoresMapa(centros,  Constantes.ESTILO_CENTRO);
		List<MapMarker> lista = Stream.concat(marcadoresClientes.stream(), marcadoresCentros.stream()).collect(Collectors.toList());
		
		ManejadorMapa.getInstancia().crearConjuntoMarcadores(lista);
		
		getContentPane().add(ManejadorMapa.getInstancia().getMapa());

		Thread t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}finally {
					label.setVisible(false);
				}
			}
		});
		
		t.start();
		
	}

	private void cargarInstanciaDeDatos() {
		situacion = new Instancia();
		centroDecidido =new ArrayList<Coordinate>();		
		coordsClientes =new ArrayList<Coordinate>();	
	}
}
