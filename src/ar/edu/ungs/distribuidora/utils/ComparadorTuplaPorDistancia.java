package ar.edu.ungs.distribuidora.utils;

import java.util.Comparator;

import ar.edu.ungs.distribuidora.beans.Tupla;

public class ComparadorTuplaPorDistancia implements Comparator<Tupla>{

	@Override
	public int compare(Tupla o1, Tupla o2) {
		return o1.getSuma() - o2.getSuma();
	}

}
