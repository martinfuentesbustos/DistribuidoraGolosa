package ar.edu.ungs.distribuidora.utils;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.Style;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

public class ManejadorMapa {

	public static final Coordinate CENTRO_MAPA = new Coordinate(-34.544012853967104, -58.71337704081132);
	public static final int DEFAULT_ZOOM_MAPA = 14;
	
	private static ManejadorMapa instancia;
	private JMapViewer mapa;
	
	
	private ManejadorMapa() {
		mapa = new JMapViewer();
	}
	
	
	public static ManejadorMapa getInstancia() {
		if(instancia == null)
			instancia = new ManejadorMapa();
		
		return instancia;
	}
	
	public ManejadorMapa setearTamanio(int x, int y, int ancho, int alto) {
		mapa.setBounds(x, y, ancho, alto);
		return this;
	}
	
	public ManejadorMapa setearTamanio(Rectangle rectangulo) {
		mapa.setBounds(rectangulo);
		return this;
	}
	
	public ManejadorMapa visualizarControlesZoom(boolean verControles) {
		mapa.setZoomControlsVisible(verControles);
		return this;
	}

	public ManejadorMapa establecerCentro(Coordinate coordenada, int nivelZoom) {
		mapa.setDisplayPosition(coordenada, nivelZoom);
		return this;
	}
	
	public ManejadorMapa crearMarcador(String nombre, Coordinate coordenada) {
		MapMarker marcador = new MapMarkerDot(nombre, coordenada);
		mapa.addMapMarker(marcador);
		return this;
	}

	public ManejadorMapa crearMarcadorConEstilo(String nombre, Coordinate coordenada, Style estilo) {
		MapMarker marcador = new MapMarkerDot(nombre, coordenada);
		marcador.getStyle().setColor(estilo.getColor());
		marcador.getStyle().setBackColor((estilo.getBackColor()));
		mapa.addMapMarker(marcador);
		return this;
	}
	
	public ManejadorMapa crearMarcadoresConEstilo(List<MapMarker> marcadores, Style estilo) {
		mapa.setMapMarkerList(marcadores);
		return this;
	}
	
	public ManejadorMapa verPosicionDeVisualizacion(Coordinate coordenada, int nivelZoom) {
		mapa.setDisplayPosition(coordenada, nivelZoom);
		return this;
	}
	
	public ManejadorMapa verPosicionDeVisualizacion(int x, int y, int nivelZoom) {
		mapa.setDisplayPosition(x, y, nivelZoom);
		return this;
	}
	
	public ManejadorMapa crearConjuntoMarcadores(List<MapMarker> marcadores){
		mapa.setMapMarkerList(marcadores);
		return this;
	}
	
	public static Coordinate obtenerCoordenada(double latitud, double longitud) {
		return new Coordinate(latitud, longitud);
	}
	
	public void removerMarcador(MapMarker marcador) {
		mapa.removeMapMarker(marcador);
	}
	
	public void removerMarcadores() {
		mapa.removeAllMapMarkers();
	}
	
	public void removerPoligonos() {
		mapa.removeAllMapPolygons();
	}
	
	public List<MapMarker> obtenerMarcadores() {
		return mapa.getMapMarkerList();
	}
	public JMapViewer getMapa() {
		return mapa;
	}
	
	private List<Coordinate> route  (Coordinate one, Coordinate two)
	{
		List<Coordinate> route = new ArrayList<Coordinate>(Arrays.asList(one, two, two));
		
		return route;
	}

	public void dibujarPoligono(ArrayList<Coordinate> coordsClientes, ArrayList<Coordinate> centroDecidido) {
		for(int i = 0; i< coordsClientes.size();i++) {
			mapa.addMapPolygon(new MapPolygonImpl(route(coordsClientes.get(i),centroDecidido.get(i))));
		}
	}
}
