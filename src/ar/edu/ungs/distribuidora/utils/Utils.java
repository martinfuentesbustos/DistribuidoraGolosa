package ar.edu.ungs.distribuidora.utils;

import java.util.ArrayList;
import java.util.List;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.Style;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import com.google.gson.Gson;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Cliente;

public class Utils {

	public static <T> T[] obtenerObjetoDesdeJSON(String JSONString, Class<T[]> clase) {
		Gson gson = new Gson();
		T[] o = (T[]) gson.fromJson(JSONString, clase);
		return o;
	}
	
	public static List<MapMarker> obtenerMarcadoresMapa(Cliente[] clientes, Style estilo){
		List<MapMarker> marcadores = new ArrayList<MapMarker>();
		for (Cliente cliente : clientes) {
			MapMarker marker = new MapMarkerDot(cliente.getNombre(), new Coordinate(cliente.getLatitud(), cliente.getLongitud()));
			marker.getStyle().setColor(estilo.getColor());
			marker.getStyle().setBackColor(estilo.getBackColor());
			marcadores.add(marker);
		}
		return marcadores;
	}
	
	public static List<MapMarker> obtenerMarcadoresMapa(CentroDeDistribucion[] centros, Style estilo){
		List<MapMarker> marcadores = new ArrayList<MapMarker>();
		for (CentroDeDistribucion centro : centros) {
			MapMarker marker = new MapMarkerDot(centro.getNombre(), new Coordinate(centro.getLatitud(), centro.getLongitud()));
			marker.getStyle().setColor(estilo.getColor());
			marker.getStyle().setBackColor(estilo.getBackColor());
			marcadores.add(marker);
		}
		return marcadores;
	}
	
	public static MapMarker obtenerMarcadorCentro(CentroDeDistribucion centro) {
		return new MapMarkerDot(centro.getNombre(), new Coordinate(centro.getLatitud(), centro.getLongitud()));
	}
}
