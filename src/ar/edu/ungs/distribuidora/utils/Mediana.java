package ar.edu.ungs.distribuidora.utils;

import java.util.ArrayList;
import java.util.Collections;

public class Mediana {

	@SuppressWarnings("unchecked")
	public static double calcularMediana(ArrayList<Integer> distancias) {

		if(distancias == null)
			throw new IllegalArgumentException("La lista de distancias es nula!");
		
		ArrayList<Integer> distanciasCopia = (ArrayList<Integer>) distancias.clone();
		Collections.sort(distanciasCopia);

		int medio = distanciasCopia.size() / 2;
		if (distanciasCopia.size() % 2 == 1) {
			return distanciasCopia.get(medio);
		} else {
			return (distanciasCopia.get(medio - 1) + distanciasCopia.get(medio)) / 2.0;
		}
	}
}
