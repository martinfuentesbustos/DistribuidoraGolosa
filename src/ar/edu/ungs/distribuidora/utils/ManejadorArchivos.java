package ar.edu.ungs.distribuidora.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ManejadorArchivos {

	public static String leerArchivo(File ruta) {

		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			String line = null;
			
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			br.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	private static void guardarArchivo(File ruta, String jsonString) {
		FileWriter writer;
		try {
			writer = new FileWriter(ruta);
			writer.write(jsonString);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	public static void guardar(File ruta, String jsonString) {
		guardarArchivo(ruta, jsonString);
		guardarArchivoFirma(ruta.getParent());
	}

	private static void guardarArchivoFirma(String absolutePath) {
		StringBuilder sb = new StringBuilder();
		sb.append("---------------------Distribuidora - LA GOLOSA -------------------------\n");
		sb.append("------------------Solucion traida por los golosos-----------------------\n");
		sb.append("Martin Fuentes Bustos\n");
		sb.append("Nahuel Clauser\n");
		sb.append("Leonardo Lamaruggine\n");
		sb.append("---------------------------------2020------------------------------------");
		guardarArchivo(new File(absolutePath+"\\README.txt"), sb.toString());
	}

}
