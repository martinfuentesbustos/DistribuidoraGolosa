package ar.edu.ungs.distribuidora.utils;

import java.awt.Color;
import java.awt.Rectangle;

import org.openstreetmap.gui.jmapviewer.Style;

public class Constantes {

	public static final String NOMBRE_APLICACION = "Distribuidora Golosa";
	public static final String ARCHIVO_CLIENTES = "clientes.json";
	public static final String ARCHIVO_CENTROS = "centros_distribucion.json";
	public static final Style ESTILO_CLIENTE = new Style(Color.BLACK, Color.GREEN, null, null);
	public static final Style ESTILO_CENTRO = new Style(Color.BLACK, Color.RED, null, null);
	
	public static final Rectangle RECTANGULO_PANEL = new Rectangle(19, 71, 528, 384);
	public static final Rectangle RECTANGULO_MAPA = new Rectangle(RECTANGULO_PANEL.x+1, RECTANGULO_PANEL.y+1, RECTANGULO_PANEL.width-2, RECTANGULO_PANEL.height-2);
	public static final int OPCION_SELECCIONAR = 0;
	public static final int OPCION_POR_SUMA = 1;
	public static final int OPCION_POR_MEDIANA = 2;
	public static final int GUARDAR_ARCHIVO = 0;
}
