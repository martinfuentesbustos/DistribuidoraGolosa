package ar.edu.ungs.distribuidora.algoritmos;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Cliente;
import ar.edu.ungs.distribuidora.utils.Constantes;
import ar.edu.ungs.distribuidora.utils.ManejadorArchivos;
import ar.edu.ungs.distribuidora.utils.Semiverseno;
import ar.edu.ungs.distribuidora.utils.Utils;

public class Instancia {

	private Cliente[] clientes;
	
	private CentroDeDistribucion[] centros;
	
	private Map<CentroDeDistribucion, ArrayList<Integer>> distancias;
	
	
	
	public Instancia() {
		String strJsonClientes = ManejadorArchivos.leerArchivo(new File(Constantes.ARCHIVO_CLIENTES));
		String strJsonCentros = ManejadorArchivos.leerArchivo(new File(Constantes.ARCHIVO_CENTROS));
		cargarClientes(strJsonClientes);
		cargarCentros(strJsonCentros);
		this.distancias = calcularDistancias();
	}
	
	private Map<CentroDeDistribucion, ArrayList<Integer>> calcularDistancias(){
		Map<CentroDeDistribucion, ArrayList<Integer>> mapa = new HashMap<CentroDeDistribucion, ArrayList<Integer>>();
		
		for (CentroDeDistribucion centro : centros) {
			ArrayList<Integer> distancias = new ArrayList<>();
			for (Cliente cliente : clientes) {
				int distancia = Semiverseno.calcularDistancia(centro.getLongitud(), centro.getLatitud(), cliente.getLongitud(), cliente.getLatitud());
				distancias.add(distancia);
			}
			mapa.put(centro, distancias);
		}
		
		return mapa;
	}

	private void cargarCentros(String jsonString) {
		 centros = Utils.obtenerObjetoDesdeJSON(jsonString, CentroDeDistribucion[].class);
	}

	private void cargarClientes(String jsonString) {
		clientes = Utils.obtenerObjetoDesdeJSON(jsonString, Cliente[].class);
	}

	public Map<CentroDeDistribucion, ArrayList<Integer>> getDistancias() {
		return distancias;
	}

	public Integer getDistanciaACliente(CentroDeDistribucion centro, int index) {
		return  distancias.get(centro).get(index);
	}

	public int cantidadClientes() {
		return clientes.length;
	}

	public Cliente[] getClientes() {
		return clientes;
	}

	public CentroDeDistribucion[] getCentros() {
		return centros;
	}
	
}
