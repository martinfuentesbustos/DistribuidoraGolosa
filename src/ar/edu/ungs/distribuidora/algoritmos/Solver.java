package ar.edu.ungs.distribuidora.algoritmos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ar.edu.ungs.distribuidora.algoritmos.strategy.Calculador;
import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Tupla;
import ar.edu.ungs.distribuidora.utils.ComparadorTuplaPorDistancia;

public class Solver {
	
	private Instancia situacion;
	private Calculador calculador;
	
	public Solver(Instancia situacion, Calculador calculador) {
		this.situacion = situacion;
		this.calculador = calculador;
	}
	
	public Solucion resolver(int cantidadCentrosAAbrir) {
		
		if(cantidadCentrosAAbrir==0)
			throw new IllegalArgumentException("La cantidad de centros no puede ser cero!!");
		
		Solucion solucion = new Solucion();
		List<CentroDeDistribucion> centrosElegidos = new ArrayList<CentroDeDistribucion>();
		
		List<Tupla> tuplas = calculador.calcular(situacion.getDistancias());
		Collections.sort(tuplas, new ComparadorTuplaPorDistancia());
		
		List<Tupla> tuplasElegidas = tuplas.subList(0, cantidadCentrosAAbrir);
		
		for (Tupla tupla : tuplasElegidas) {
			centrosElegidos.add(tupla.getCentro());
		}

		solucion.setCentrosElegidos(centrosElegidos);
		solucion.setCostoTotal(calcularCosto(centrosElegidos));
		solucion.setCentrosMasCercanos(centroMasCercano(centrosElegidos));
		
		return solucion;
	}
	
	private int calcularCosto(List<CentroDeDistribucion> centrosElegidos) {
		
		int sumaCosto = 0;
		
		for (int i = 0; i < situacion.cantidadClientes(); i++) {
			int menorDistancia = Integer.MAX_VALUE;
			for (CentroDeDistribucion centro : centrosElegidos) {
				int distancia = situacion.getDistanciaACliente(centro, i);
				if(distancia < menorDistancia) {
					menorDistancia = distancia;
				}
			}
			sumaCosto += menorDistancia;
		}
		return sumaCosto;
	}
	
	public List <CentroDeDistribucion> centroMasCercano(List<CentroDeDistribucion> centrosElegidos)
	{
		List<CentroDeDistribucion> centrosMasCercano = new ArrayList<CentroDeDistribucion >() ;
			
			for(int i = 0; i < situacion.cantidadClientes(); i++) 
			{
				centrosMasCercano.add(i,masCercano(i,centrosElegidos));
				
			}
		return centrosMasCercano;
	}
	
	private CentroDeDistribucion masCercano(int i, List<CentroDeDistribucion> centrosElegidos )
	{
		CentroDeDistribucion cercano = new CentroDeDistribucion();
		int menorDistancia = Integer.MAX_VALUE;
		
		for (CentroDeDistribucion centro : centrosElegidos) 
		{
			int distancia = situacion.getDistanciaACliente(centro, i);
			if(distancia < menorDistancia) 
			{
				menorDistancia = distancia;
				cercano = centro;
			}
			
		}
		
		return cercano;
	}

}
