package ar.edu.ungs.distribuidora.algoritmos.strategy;

import java.util.ArrayList;
import java.util.Map;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Tupla;
import ar.edu.ungs.distribuidora.utils.Mediana;

public class CalculadorPorMediana implements Calculador {

	@Override
	public ArrayList<Tupla> calcular(Map<CentroDeDistribucion, ArrayList<Integer>> mapa) {
		ArrayList<Tupla> tuplas = new ArrayList<Tupla>();

		for (Map.Entry<CentroDeDistribucion, ArrayList<Integer>> entry : mapa.entrySet()) {
			CentroDeDistribucion centro = entry.getKey();
			ArrayList<Integer> distancias = entry.getValue();
			Integer medianaDistancias = (int) Mediana.calcularMediana(distancias);
			tuplas.add(new Tupla(centro, medianaDistancias));
		}
		return tuplas;
	}
}
