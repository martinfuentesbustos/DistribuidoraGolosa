package ar.edu.ungs.distribuidora.algoritmos.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Tupla;

public interface Calculador {

	public List<Tupla> calcular(Map<CentroDeDistribucion, ArrayList<Integer>> mapa);
}
