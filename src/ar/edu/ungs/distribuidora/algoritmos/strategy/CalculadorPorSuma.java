package ar.edu.ungs.distribuidora.algoritmos.strategy;

import java.util.ArrayList;
import java.util.Map;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;
import ar.edu.ungs.distribuidora.beans.Tupla;

public class CalculadorPorSuma implements Calculador{

	@Override
	public ArrayList<Tupla> calcular(Map<CentroDeDistribucion, ArrayList<Integer>> mapa) {
		
		ArrayList<Tupla> tuplas = new ArrayList<Tupla>();
		
		for (Map.Entry<CentroDeDistribucion, ArrayList<Integer>> entry : mapa.entrySet()) {
			CentroDeDistribucion centro = entry.getKey();
			int sumaDistancias = 0;
			ArrayList<Integer> distancias = entry.getValue();
			for (Integer distancia : distancias) {
				sumaDistancias += distancia;
			}
			tuplas.add(new Tupla(centro, sumaDistancias));
		}
		return tuplas;
	}

}
