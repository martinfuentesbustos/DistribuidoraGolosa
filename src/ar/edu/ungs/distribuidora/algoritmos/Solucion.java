package ar.edu.ungs.distribuidora.algoritmos;

import java.util.List;

import ar.edu.ungs.distribuidora.beans.CentroDeDistribucion;

public class Solucion {

	private List<CentroDeDistribucion> centrosElegidos;
	private Integer costoTotal;
	private List<CentroDeDistribucion> centrosMasCercanos;
	
	public List<CentroDeDistribucion> getCentrosMasCercanos() {
		return centrosMasCercanos;
	}
	public void setCentrosMasCercanos(List<CentroDeDistribucion> centroMasCercano) {
		this.centrosMasCercanos = centroMasCercano;
	}
	
	public List<CentroDeDistribucion> getCentrosElegidos() {
		return centrosElegidos;
	}
	public void setCentrosElegidos(List<CentroDeDistribucion> centrosElegidos) {
		this.centrosElegidos = centrosElegidos;
	}
	public Integer getCostoTotal() {
		return costoTotal;
	}
	public void setCostoTotal(Integer costoTotal) {
		this.costoTotal = costoTotal;
	}
	
}
